# phonenumber-normalizer

Command line tool for "normalizing" telephone numbers contained in a CSV file.

## Requirements

PHP 7.0+, Composer

## Installation

Checkout this repository and install dependencies using Composer:

```
composer install
```

## CLI-Usage

Make sure the file `normalize` is executable. Then run `./normalize` to see a list of required and optional parameters:

```
Required Arguments:
        -i inputFilename, --inputFilename inputFilename
                The path to the input file containing phone numbers to process
        -o outputFilename, --outputFilename outputFilename
                The path to the file to write the output to
        -c defaultCountry, --country defaultCountry (default: DE)
                The ISO-3166-1 Alpha-2 code of the default country to use
        -n inputNumberColumn, --inputNumberColumn inputNumberColumn
                The index number or name of the column of the input file that contains the phone number (or only the extension)

Optional Arguments:
        --inputAreaCodeColumn inputAreaCodeColumn
                The index number or name of the column of the input file that contains the phone area code
        --inputCountryCodeColumn inputCountryCodeColumn
                The index number or name of the column of the input file that contains the country code
        --outputNumberColumn outputNumberColumn (default: 1)
                The index number or name of the output file's column to write the normalized phone number to. If true, appends a new column.
        --outputAreaCodeColumn outputAreaCodeColumn
                The index number of name of the output file's column to write the normalized area code to. If true, appends a new column.
        --outputCountryCodeColumn outputCountryCodeColumn
                The index number of name of the output file's column to write the normalized country code to. If true, appends a new column.
        -f outputNumberFormat, --outputNumberFormat outputNumberFormat (default: INTERNATIONAL)
                The format of the normalized phone number, if no separate output columns for area and country codes are being used. Can be either: E164, NATIONAL, INTERNATIONAL, RFC3966
        -h firstRowContainsColumnNames, --firstRowContainsColumnNames firstRowContainsColumnNames (default: 1)
                Whether the input file's first record contains the header names
        -v removeRowsWithInvalidNumbers, --removeRowsWithInvalidNumbers removeRowsWithInvalidNumbers
                If true, the output files will contain only records valid / parseable phone numbers
```