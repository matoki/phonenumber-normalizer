<?php

namespace Matoki\PhonenumberNormalizer;

use League\Csv\CannotInsertRecord;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Monolog\Logger;
use Psr\Log\AbstractLogger;

class Runner
{

    protected $job;

    protected $phoneNumberUtil;

    protected $log;

    function __construct(Job $job, Logger $log)
    {
        $this->job = $job;
        $this->phoneNumberUtil = PhoneNumberUtil::getInstance();
        $this->log = $log;
    }

    public function handle()
    {
        try {
            if ($this->job->isFirstRowContainsColumnNames() && $this->job->getHeader()) {
                $this->job->getOutput()->insertOne($this->job->getHeader());
            }
            foreach ($this->job->getInput() as $record) {
                $phoneNumber = $this->getInputPhoneNumber($record);
                $output = $record;
                try {
                    $phoneNumberProto = $this->phoneNumberUtil->parse($phoneNumber, $this->job->getDefaultCountry()['alpha2']);
                    if (!$this->phoneNumberUtil->isValidNumber($phoneNumberProto) && $this->job->isRemoveRowsWithInvalidNumbers()) {
                        $this->log->warning('Phone number "' . $phoneNumber . '" is not valid');
                        continue;
                    }
                    if ($this->job->getOutputCountryCodeColumn()) {
                        $output[$this->job->getOutputCountryCodeColumn()] = $phoneNumberProto->getCountryCode();
                        if ($this->job->getOutputAreaCodeColumn()) {
                            $output[$this->job->getOutputAreaCodeColumn()] = $phoneNumberProto->getNationalNumber();
                            $output[$this->job->getOutputNumberColumn()] = $phoneNumberProto->getExtension();
                        } else {
                            $output[$this->job->getOutputNumberColumn()] = $phoneNumberProto->getNationalNumber() . ' ' . $phoneNumberProto->getExtension();
                        }
                    } elseif ($this->job->getOutputAreaCodeColumn()) {
                        $output[$this->job->getOutputAreaCodeColumn()] = $phoneNumberProto->getNationalNumber();
                        $output[$this->job->getOutputNumberColumn()] = $phoneNumberProto->getExtension();
                    } else {
                        $output[$this->job->getOutputNumberColumn()] = $this->phoneNumberUtil->format($phoneNumberProto, $this->job->getOutputNumberFormat());
                    }
                    $this->log->info('Normalized phone number "' . $phoneNumber . '" to "' . $this->phoneNumberUtil->format($phoneNumberProto, PhoneNumberFormat::INTERNATIONAL) . '"');
                    $this->job->getOutput()->insertOne($output);
                } catch (NumberParseException $e) {
                    $this->log->warning('Phone number "' . $phoneNumber . '" cannot be parsed');
                    if (!$this->job->isRemoveRowsWithInvalidNumbers()) {
                        $this->job->getOutput()->insertOne($output);
                    }
                }
            }
        } catch (CannotInsertRecord $e) {
            $this->log->warning('Cannot write output to file');
        }
    }

    public function getInputPhoneNumber(array $record): string
    {
        $keys = array_keys($record);
        $phoneNumber = $record[$keys[$this->job->getInputNumberColumn()]];
        if (!is_null($this->job->getInputAreaCodeColumn())) {
            $phoneNumber = $record[$keys[$this->job->getInputAreaCodeColumn()]] . $phoneNumber;
        }
        if (!is_null($this->job->getInputCountryCodeColumn())) {
            $phoneNumber = $record[$keys[$this->job->getInputCountryCodeColumn()]] . $phoneNumber;
        }
        return $phoneNumber;
    }

}