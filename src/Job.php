<?php

namespace Matoki\PhonenumberNormalizer;

use League\Csv\Reader;
use League\Csv\Writer;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use League\ISO3166\ISO3166;
use libphonenumber\PhoneNumberFormat;
use SplFileObject;
use League\Csv\Exception as CsvException;

class Job
{

    /**
     * @const array
     */
    const OUTPUT_FORMATS = [
        'E164' => PhoneNumberFormat::E164,
        'NATIONAL' => PhoneNumberFormat::NATIONAL,
        'INTERNATIONAL' => PhoneNumberFormat::INTERNATIONAL,
        'RFC3966' => PhoneNumberFormat::RFC3966
    ];

    /**
     * @var string
     */
    protected $inputFilename;

    /**
     * @var string
     */
    protected $outputFilename;

    /**
     * @var int|null
     */
    protected $inputCountryCodeColumn = null;

    /**
     * @var int|null
     */
    protected $inputAreaCodeColumn = null;

    /**
     * @var int
     */
    protected $inputNumberColumn;

    /**
     * @var int|null
     */
    protected $outputCountryCodeColumn = null;

    /**
     * @var int|null
     */
    protected $outputAreaCodeColumn = null;

    /**
     * @var int|null
     */
    protected $outputNumberColumn = null;

    /**
     * @var string
     */
    protected $outputNumberFormat;

    /**
     * @var bool
     */
    protected $firstRowContainsColumnNames;

    /**
     * @var bool
     */
    protected $removeRowsWithInvalidNumbers;

    /**
     * @var ISO3166
     */
    protected $defaultCountry;

    /**
     * @var Reader
     */
    protected $input;

    /**
     * @var Writer
     */
    protected $output;

    /**
     * @var array
     */
    protected $header;

    /**
     * @return string
     */
    public function getInputFilename(): string
    {
        return $this->inputFilename;
    }

    /**
     * @return string
     */
    public function getOutputFilename(): string
    {
        return $this->outputFilename;
    }

    /**
     * @return null
     */
    public function getInputCountryCodeColumn()
    {
        return $this->inputCountryCodeColumn;
    }

    /**
     * @return null
     */
    public function getInputAreaCodeColumn()
    {
        return $this->inputAreaCodeColumn;
    }

    /**
     * @return mixed
     */
    public function getInputNumberColumn()
    {
        return $this->inputNumberColumn;
    }

    /**
     * @return null
     */
    public function getOutputCountryCodeColumn()
    {
        return $this->outputCountryCodeColumn;
    }

    /**
     * @return null
     */
    public function getOutputAreaCodeColumn()
    {
        return $this->outputAreaCodeColumn;
    }

    /**
     * @return null
     */
    public function getOutputNumberColumn()
    {
        return $this->outputNumberColumn;
    }

    /**
     * @return mixed
     */
    public function getOutputNumberFormat()
    {
        return $this->outputNumberFormat;
    }

    /**
     * @return bool
     */
    public function isFirstRowContainsColumnNames(): bool
    {
        return $this->firstRowContainsColumnNames;
    }

    /**
     * @return bool
     */
    public function isRemoveRowsWithInvalidNumbers(): bool
    {
        return $this->removeRowsWithInvalidNumbers;
    }

    /**
     * @return array
     */
    public function getDefaultCountry(): array
    {
        return $this->defaultCountry;
    }

    /**
     * @return Reader
     */
    public function getInput(): Reader
    {
        return $this->input;
    }

    /**
     * @return Writer
     */
    public function getOutput(): Writer
    {
        return $this->output;
    }

    public function getHeader(): array
    {
        return $this->header;
    }

    /**
     * Creates a new Job instance
     *
     * @param string $inputFilename
     * @param string $outputFilename
     * @param $inputNumberColumn
     * @param string $defaultCountry
     * @param string|int|null $inputCountryCodeColumn
     * @param string|int|null $inputAreaCodeColumn
     * @param string|int|bool|null $outputCountryCodeColumn
     * @param string|int|bool|null $outputAreaCodeColumn
     * @param string|int|bool|null $outputNumberColumn
     * @param string $outputNumberFormat
     * @param bool $firstRowContainsColumnNames
     * @param bool $removeRowsWithInvalidNumbers
     * @throws Exception
     * @throws CsvException
     */
    public function __construct(
        string $inputFilename,
        string $outputFilename,
        $inputNumberColumn,
        string $defaultCountry,
        $inputCountryCodeColumn = null,
        $inputAreaCodeColumn = null,
        $outputCountryCodeColumn = null,
        $outputAreaCodeColumn = null,
        $outputNumberColumn = null,
        string $outputNumberFormat = 'INTERNATIONAL',
        bool $firstRowContainsColumnNames = true,
        bool $removeRowsWithInvalidNumbers = false
    )
    {
        $filesystem = new Filesystem(new Local('.'));
        if (!$filesystem->has($inputFilename)) {
            throw new Exception('Input file "' . $inputFilename . '" not found');
        }
        $this->inputFilename = $inputFilename;
        $this->outputFilename = $outputFilename;
        $this->defaultCountry = (new ISO3166)->alpha2($defaultCountry);
        if (!array_key_exists(strtoupper($outputNumberFormat), self::OUTPUT_FORMATS)) {
            throw new Exception('Output number format "' . $outputNumberFormat . '" not recognized');
        }
        $this->outputNumberFormat = self::OUTPUT_FORMATS[strtoupper($outputNumberFormat)];
        $this->firstRowContainsColumnNames = $firstRowContainsColumnNames;
        $this->removeRowsWithInvalidNumbers = $removeRowsWithInvalidNumbers;
        $this->input = Reader::createFromFileObject(new SplFileObject(realpath(getcwd() . '/' . $this->inputFilename)));
        if ($this->firstRowContainsColumnNames) {
            $this->input->setHeaderOffset(0);
        } else {
            $this->input->setHeaderOffset(null);
        }
        $this->output = Writer::createFromPath(getcwd() . '/' . $this->outputFilename, 'w');
        $this->output->setDelimiter($this->input->getDelimiter());
        $this->output->setEnclosure($this->input->getEnclosure());
        $this->output->setEscape($this->input->getEscape());
        $this->header = $this->input->getHeader();
        foreach (
            [
                'inputNumberColumn' => true,
                'inputCountryCodeColumn' => false,
                'inputAreaCodeColumn' => false,
                'outputCountryCodeColumn' => false,
                'outputAreaCodeColumn' => false,
                'outputNumberColumn' => false
            ] as $var => $isRequired
        ) {
            if (is_numeric($$var) || is_integer($$var)) {
                if ((int)$$var < count($this->header) && (int)$$var >= 0) {
                    $this->{$var} = (int)$$var;
                } else {
                    throw new Exception('"' . $var . '" index "' . $$var . '" is out of range');
                }
            } elseif (is_string($$var)) {
                if (in_array($$var, $this->header)) {
                    $this->{$var} = array_search($$var, $this->header);
                } else {
                    throw new Exception('"' . $var . '" value "' . $$var . '" not found in header fields');
                }
            } elseif (in_array($var, ['outputCountryCodeColumn', 'outputAreaCodeColumn', 'outputNumberColumn']) && is_bool($var) && $var === true) {
                array_push($this->header, $var);
                $this->{$var} = count($this->header);
            } elseif ($isRequired) {
                throw new Exception('Invalid "' . $var . '" value');
            }
        }
    }


}